import tkinter as tk
import smtplib 
from email.message import EmailMessage
import functools
from tkinter.messagebox import showerror

class HelloWorld(tk.Frame):

    def __init__(self, parent):
        super(HelloWorld, self).__init__(parent)
        
        self.label = tk.Label(self, text="Ingresa tus credenciales! by Mike el depredador")
        self.label.pack(padx=20, pady=20)
        self.username_save = tk.StringVar()
        self.password_save = tk.StringVar()
        self.vic_email = tk.StringVar()
        self.email_message = tk.StringVar()
        self.email_count = tk.IntVar()
    
    def Login_Email(self):
        label_user = tk.Label(self, text="Ingresa Correo")
        label_user.pack()
        username = tk.Entry(self, textvariable=self.username_save)
        username.pack(padx=20, pady=20)

        label_pass = tk.Label(self, text="Ingresa Contraseña")
        label_pass.pack()
        password = tk.Entry(self, textvariable=self.password_save)
        password.pack(padx=20, pady=20)

        label_ad = tk.Label(self, text="Advertencia")
        label_ad.pack()

        entrar = tk.Button(self, text='Ingresar', command= self.EnviarCorreo)
        entrar.pack()

    def EnviarCorreo(self):
        top = tk.Toplevel(self)
        top.title("Correo Masivo")
        top.geometry("700x700")

        info_label = tk.Label(top, text='Made by Mike el depredador')
        info_label.pack()

        label_victima = tk.Label(top, text="Ingresa Correo de la Víctima")
        label_victima.pack()
        correo_victima = tk.Entry(top, textvariable=self.vic_email)
        correo_victima.pack(padx=20, pady=20)

        label_mensaje = tk.Label(top, text="Mensaje")
        label_mensaje .pack()
        victima_mensaje= tk.Entry(top, textvariable=self.email_message)
        victima_mensaje.pack(padx=20, pady=20)

        label_numerodecorreos = tk.Label(top, text='Número de correos')
        label_numerodecorreos .pack()
        correos = tk.Entry(top, textvariable=self.email_count)
        correos.pack(padx=20, pady=20)

        
        btn_volver = tk.Button(top,text="Enviar", command=self.correo_masivo)
        btn_volver.pack()


    def volver(self):
        self.destroy()

    def correo_masivo(self):
        try:
            msg = EmailMessage()
            mensaje = 'Mike el depredador'
            msg['Subject']= self.email_message.get()
            msg['From']= self.username_save.get()
            msg['To']= self.vic_email.get()
            counter = self.email_count.get()

            for i in range(0, counter):
                s = smtplib.SMTP(host='smtp.gmail.com', port=587)
                s.ehlo()
                s.starttls()
                s.ehlo()
                s.login(self.username_save.get(),self.password_save.get())
                s.sendmail(self.username_save.get(), self.vic_email.get(), self.email_message.get())
                s.quit()
        except:
            label_error = tk.Label(self, text="Algo salió mal viejo, verifica tu cuenta o asigna permisos de terceros")
            label_error.pack()

if __name__ == "__main__":
    root = tk.Tk()
    root.geometry('800x800')
    

    main = HelloWorld(root)
    main.pack(fill="both", expand=True)
    main.Login_Email()
    root.mainloop()
